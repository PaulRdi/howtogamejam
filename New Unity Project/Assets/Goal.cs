﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {

    public string SceneToLoad;

	void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            SceneManager.LoadScene(SceneToLoad);

    }
}
